LDFLAGS += -lmraa
CFLAGS += -I libservo
all:
	@echo type: led, softled, myblink, cycle_pwm or stepper

led:	led_pwm.c
	gcc -o led led_pwm.c -lmraa
	./led

stepper:	stepper.c
	gcc -o stepper stepper.c -lmraa
	./stepper

softled:	softled.c
	gcc -o softled softled.c -lmraa
	./softled

myblink:	myblink.c
	gcc -o myblink myblink.c -lmraa
	./myblink

cycle_pwm:	cycle_pwm.c
	gcc -o cycle_pwm $(CFLAGS) cycle_pwm.c -lmraa
	./cycle_pwm


# Edison Playground

Sample edison C programs to deal with LEDs, GPIO and PWM.

**cycle_pwm:** Servo sweep.

**servo_goto:** Goes to desired location.

**stepper:** Sample stepper motor driver.

To run, just type `make target` where `target` is the the program name without the .c extension.
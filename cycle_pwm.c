#include <unistd.h>
#include "mraa.h"
#include "df15rmg.h"

/*
static const int PWM_INTERVAL = 3000;
const float MIN_DUTY = 0.134;
const float MAX_DUTY = 0.85;

//* Enter position as between 0 and 1 
float pos_to_duty(float pos)
{
         if(pos < 0)
         {
                 printf("Warning, position cannot be smaller than 0\r\n");
                 return MIN_DUTY;
         }
         if(pos > 1)
         {
                 printf("Warning, position cannot be bigger than 1\r\n");
                 return MAX_DUTY;
         }
         float factor = MAX_DUTY - MIN_DUTY;

         return MIN_DUTY + (pos * factor);
}
*/

int
main()
{
    mraa_init();
    mraa_pwm_context pwm;
    pwm = mraa_pwm_init(0);
    if (pwm == NULL) {
        return 1;
    }
    mraa_pwm_period_us(pwm, PWM_INTERVAL);
    mraa_pwm_enable(pwm, 1);

    float position = 0.0f;
    float duty;

    while (1) {
        duty = pos_to_duty(position);
        mraa_pwm_write(pwm, duty);
        usleep(30000);
        position = position + 0.01f;
        if (position >= 1.0f) {
            position = 0.0f;
        }
        float output = mraa_pwm_read(pwm);
    }
    return 0;
}

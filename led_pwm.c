/* Blinky test using mraa */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>

#include "mraa.h"

#define LED_PIN 0

#define LED_PIN 0


//#define SLEEP_US 300000 //0.3s
#define SLEEP_US 1000000 //0.3s

//#define PWM_PERIOD 1000 //1kHz
#define PWM_PERIOD 10000

int running = 0;

void sig_handler(int signo)
{
    if ( signo == SIGINT ) {
        printf("Closing GPIO\n", LED_PIN);
        running = -1;
    }
}

int main()
{
    /* Initialize mraa */
    mraa_result_t r = MRAA_SUCCESS;
    mraa_init();

    float pwmval = 0;
    /* Create access to GPIO pin */
    mraa_pwm_context led_pwm;

    led_pwm = mraa_pwm_init(LED_PIN);

    if ( led_pwm == NULL ) {
        fprintf(stderr, "Error opening GPIO\n");
        exit(1);
    }

    mraa_pwm_period_us(led_pwm, PWM_PERIOD);
    mraa_pwm_enable(led_pwm, 1);

    /* Create signal handler so we can exit gracefully */
    signal(SIGINT, sig_handler);

    /* Turn LED off and on forever until SIGINT (Ctrl+c) */
    while ( running == 0 ) {

        pwmval = 0.0;
        printf("setting pwm %f\r\n", pwmval);
        r = mraa_pwm_write(led_pwm, 0);
        if ( r != MRAA_SUCCESS ) {
            mraa_result_print(r);
        }
        usleep(SLEEP_US);

        pwmval = 0.9;
        printf("setting pwm %f\r\n", pwmval);
        r = mraa_pwm_write(led_pwm, 0);
        if ( r != MRAA_SUCCESS ) {
            mraa_result_print(r);
        }
        usleep(SLEEP_US);
    }

    /* Clean up GPIO and exit */
    //r = mraa_gpio_close(gpio);
    //if ( r != MRAA_SUCCESS ) {
    //    mraa_result_print(r);
    //}
    mraa_pwm_enable(led_pwm, 0);

    return r;
}

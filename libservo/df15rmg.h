#ifndef __DF15RMG_H__
#define __DF15RMG_H__



static const int PWM_INTERVAL = 3000;
const float MIN_DUTY = 0.134;
const float MAX_DUTY = 0.85;

/* Enter position as between 0 and 1 */
float pos_to_duty(float pos)
{
         if(pos < 0)
         {
                 printf("Warning, position cannot be smaller than 0\r\n");
                 return MIN_DUTY;
         }
         if(pos > 1)
         {
                 printf("Warning, position cannot be bigger than 1\r\n");
                 return MAX_DUTY;
         }
         float factor = MAX_DUTY - MIN_DUTY;

         return MIN_DUTY + (pos * factor);
}

#endif

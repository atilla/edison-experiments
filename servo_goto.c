#include <unistd.h>
#include "mraa.h"
//#include "libservo/df15rmg"
#include "df15rmg.h"

int main(int argc, char* argv[])
{
    if (argc < 3)
    {
        printf(" Please enter a pwm pin id (PWM0-20, PWM1-14, PWM2-0, PWM3-21)\r\n");
	printf(" and a position (float between 0.0 and 1.0)\r\n\r\n");
	printf("   Example: servo_goto 20 0.5\r\n\r\n");
	return 2;
    }

    int pwmId = atof(argv[1]);
    float arg = atof(argv[2]);

    mraa_init();
    mraa_pwm_context pwm;

    pwm = mraa_pwm_init(pwmId);
    if (pwm == NULL) {
        return 1;
    }
    mraa_pwm_period_us(pwm, PWM_INTERVAL);
    mraa_pwm_enable(pwm, 1);

    float position = 0.0f;
    float duty;

    duty = pos_to_duty(arg);
    mraa_pwm_write(pwm, duty);
    usleep(30000);

    return 0;
}

/* Blinky test using mraa */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>

#include "mraa.h"

#define LED_PIN 8

#define DIR_PIN 4
#define STEP_PIN 0


//#define SLEEP_US 300000 //0.3s
#define SLEEP_US 1000

int running = 0;

void sig_handler(int signo)
{
    if ( signo == SIGINT ) {
        printf("Closing GPIO\n", LED_PIN);
        running = -1;
    }
}

int main()
{
    /* Initialize mraa */
    mraa_result_t r = MRAA_SUCCESS;
    mraa_init();

    /* Create access to GPIO pin */
    mraa_gpio_context dir_pin;
    mraa_gpio_context step_pin;

    dir_pin = mraa_gpio_init(DIR_PIN);
    step_pin = mraa_gpio_init(STEP_PIN);

    if ( dir_pin == NULL ) {
        fprintf(stderr, "Error opening GPIO\n");
        exit(1);
    }
    if ( step_pin == NULL ) {
        fprintf(stderr, "Error opening GPIO\n");
        exit(1);
    }

    /* Set GPIO direction to out */
    r = mraa_gpio_dir(step_pin, MRAA_GPIO_OUT);
    if ( r != MRAA_SUCCESS ) {
        mraa_result_print(r);
    }
    r = mraa_gpio_dir(dir_pin, MRAA_GPIO_OUT);
    if ( r != MRAA_SUCCESS ) {
        mraa_result_print(r);
    }

    /* Create signal handler so we can exit gracefully */
    signal(SIGINT, sig_handler);

    mraa_gpio_write(dir_pin, 1);

    /* Turn LED off and on forever until SIGINT (Ctrl+c) */
    while ( running == 0 ) {

        r = mraa_gpio_write(step_pin, 0);
        if ( r != MRAA_SUCCESS ) {
            mraa_result_print(r);
        }
        usleep(SLEEP_US);

        r = mraa_gpio_write(step_pin, 1);
        if ( r != MRAA_SUCCESS ) {
            mraa_result_print(r);
        }
        usleep(SLEEP_US);
    }

    /* Clean up GPIO and exit */
    //r = mraa_gpio_close(gpio);
    //if ( r != MRAA_SUCCESS ) {
    //    mraa_result_print(r);
    //}
    mraa_gpio_write(dir_pin, 0);

    return r;
}
